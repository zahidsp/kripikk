<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tes', function () {
    return view('layouts.tes');
});

Route::get('/api/products',function(){
	return App\Product::where('name','LIKE','%'.request('q').'%')->paginate(10);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function (){

    Route::get('autocomplete', 'ProductsController@autocomplete')->name('autocomplete');
    Route::resource('customers', 'CustomersController');
    Route::resource('products', 'ProductsController');
    Route::resource('reports', 'ReportController');
    Route::resource('transactions', 'TransactionsController');
    Route::resource('costs', 'ProductionCostsController');
    Route::resource('units', 'UnitProductsController');

    Route::get('/findPrice','TransactionsController@findPrice');

    Route::get('/transactionData','ProductionCostsController@transactionData');
    Route::get('/unitPrice','ProductionCostsController@unitPrice');

    Route::get('invoice','TransactionsController@invoice');
    Route::post('/transactions/postInvoice','TransactionsController@postInvoice');
});











Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
