<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->char('invoice_no', 10)->unique();
            $table->string('order_name');

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('customer_id');

            $table->unsignedInteger('total_order');
            $table->unsignedInteger('payment');
            $table->unsignedInteger('kurang');
            $table->text('notes')->nullable();
            $table->enum('status', ['Dalam Proses', 'Selesai']);	

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');

            $table->timestamps();
            $table->timestamp('tanggal_jadi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
