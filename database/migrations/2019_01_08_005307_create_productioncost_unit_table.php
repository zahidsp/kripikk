<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductioncostUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productioncost_unit', function (Blueprint $table) {
            $table->unsignedInteger('production_cost_id');
            $table->unsignedInteger('unit_product_id');
            $table->unsignedInteger('quantity');
            $table->unsignedInteger('jumlah_harga');
            $table->timestamps();

            $table->foreign('production_cost_id')->references('id')->on('production_costs');
            $table->foreign('unit_id')->references('id')->on('unit_products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productioncost_unit');
    }
}
