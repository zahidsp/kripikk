<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 5; $i++) { 
            
            DB::table('customers')->insert([
                'name' => $faker-> name,
                'address' => $faker-> address,
                'phone' => $faker-> tollFreePhoneNumber
            ]);
        }
    }
}
