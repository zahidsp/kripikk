<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionCost extends Model
{
    protected $fillable = [
        'id',
        'order_id',
        'order_name',
        'subtotal',
        'diskon',
        'total_cost',
        'potensi_laba'
    ];

    public function Order()
    {
        return $this->belongsTo('App\Transaction','order_id');
    }

    public function Units()
    {
        return $this->belongsToMany('App\UnitProduct', 'productioncost_unit')
        ->withTimeStamps()
        ->withPivot(['quantity','jumlah_harga'])
        ;
    }
}
