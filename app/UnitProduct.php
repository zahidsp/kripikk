<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitProduct extends Model
{

    // protected $table = 'unit_products';

    protected $fillable = [
        'id',
        'name',
        'price'
    ];

    public function Products(){
        return $this->hasMany('App\Product','unit_id');
    }

    public function Costs()
    {
        return $this->belongsToMany('App\ProductionCost','productioncost_unit');
    }
}
