<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Transaction;
use App\ProductionCost;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    

    public function index()
    {        
        // function flatten(array $items, array $flattened =[]) {
        //     foreach ($items as $item) {
        //         if (is_array($item)){
        //             $flattened = flatten($item, $flattened);
        //             continue;
        //         }

        //         $flattened[] = $item;
        //     }
        //     return $flattened;
        // }

        $status = "Selesai";
        // $transactions=Transaction::where('status',$status)->get();
        // $productionCosts= ProductionCost::all();

        // $cost = ProductionCost::with('Order')->find(9);
        $transactions = Transaction::with('Cost')
                        ->where('status',$status)
                        // ->whereMonth('created_at', '1')
                        ->get();
                        // ->toArray();

        // foreach($transactions as $value)
        // {
        //     $transaction[] = $value->toArray();
        // }

       
        // $tes = flatten($transaction);

        // dd($transactions->toArray());
        // dd($tes);

        // $month = 1;
        // $transactions = DB::table('transactions')
        //     ->join('production_costs', 'transactions.id', '=', 'production_costs.order_id')
        //     ->select('transactions.*', 'production_costs.*') 
        //     ->where('status',$status)
        //     ->whereMonth('created_at', '1')
        //     ->get();
            
        // dd($transactions->toArray());

        return view('reports.index', compact(['transactions','cost']));
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bulan = array("Januari","Februari",'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $status = "Selesai";
        $month = $request->input('month');
        // dd($month);

        $transactions = Transaction::with('Cost')
        ->where('status',$status)
        ->whereMonth('updated_at', $month)
        ->get();

        for ($i=1; $i <= $month; $i++) { 
           $tes = $bulan[$i-1];
        }
        // dd($tes);
        if($transactions){
            return view('reports.show', compact(['transactions','cost', 'tes']));
            // -> with('success', 'Product created successfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

}
