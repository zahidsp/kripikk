<?php

namespace App\Http\Controllers;

use App\UnitProduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class UnitProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $units = DB::table('unit_products')->get();
        $units = UnitProduct::all();

        // dd($units);
        return view('unit_products.index',['units' => $units]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unit_products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $units = UnitProduct::create([
            'name' => $request->input('name'),
            'price' => $request->input('price')
        ]);

        if ($units) {
            # code...
            return redirect()->route('units.index')
            ->with('success', 'Unit created successfully');
        }
        return back()->withInput()->with('errors', 'operation failed');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UnitProduct  $unitProduct
     * @return \Illuminate\Http\Response
     */
    public function show($unitProduct)
    {
        $units = UnitProduct::find($unitProduct);
        // dd($units);
        // $units = UnitProduct::where('id', $unitProduct-> id);
        // $units = DB::table('unit_products')->where('id',$unitProduct-> id);
        return view('unit_products.show', ['units' => $units]);
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UnitProduct  $unitProduct
     * @return \Illuminate\Http\Response
     */
    public function edit($unitProduct)
    {
        $units = UnitProduct::find($unitProduct);
        return view('unit_products.edit',['units' => $units]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnitProduct  $unitProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $unitProduct)
    {
        $unitUpdate = UnitProduct::where('id', $unitProduct)
        ->update([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
        ]);

        if ($unitUpdate) {
            return redirect()->route('units.index')
            ->with('success', 'Update Successfully');
        }
        
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnitProduct  $unitProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $deleteUnit = UnitProduct::find($request-> unit_id);
        if ($deleteUnit->delete()) {
            
            return redirect()->route('units.index')
            ->with('success', 'Unit Produk deleted successfully');
        }

        return back()->withInput()->with('error','operation failed');
    }
}
