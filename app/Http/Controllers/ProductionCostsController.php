<?php

namespace App\Http\Controllers;

use App\ProductionCost;
use App\Transaction;
use App\UnitProduct;


use Illuminate\Http\Request;

class ProductionCostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $costs = ProductionCost::all();

        // dd($transactions);
        return view('production_costs.index',['costs'=> $costs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transactions = Transaction::all();
        $units = UnitProduct::all();
        return view('production_costs.create',['transactions'=>$transactions,'units'=>$units]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idTrans = $request->input('transaction');
        $transaction = Transaction::where('id',$idTrans)->get();
        // $orderName = $transaction->invoice_no;       

        foreach ($transaction as $key) {
           $orderName = $key -> order_name;
        }

        // dd($orderName);

        $costs = ProductionCost::create([
            'order_id' => $request->input('transaction'),
            'order_name' => $orderName,
            'subtotal' => $request->input('subtotal'),
            'diskon' => $request->input('diskon'),
            'total_cost' => $request->input('total'),
            'potensi_laba' => $request->input('potensi_laba')

        ]);

        $lastCost = ProductionCost::orderBy('created_at', 'desc')->first();
        $idLastCost = $lastCost-> id;

        // dd($idLastCost);

        $forCost= ProductionCost::findOrFail($idLastCost);
        // $forProduct= ProductionCost::findOrFail($idProduct);

        for ($i = 1; $i <= count($request->unitname); $i++) {
            if (!empty($request->unitname[$i])) {
            $products=$forCost->Units()->attach($request->unitname[$i],['quantity'=>$request->quantity[$i],'jumlah_harga'=>$request->jumlah_harga[$i]]);                
            }
        }

        if($costs){
            return redirect() -> route('costs.index')
            -> with('success', 'Production Cost created successfully');
        }
        return back() -> withInput() -> with('errors','operation failed');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductionCost  $productionCost
     * @return \Illuminate\Http\Response
     */
    public function show( $productionCost)
    {
        $costs = ProductionCost::findorfail($productionCost);
        $transaction = $costs -> Order;
        // $unit = $costs->Units()->attach(2,['quantity'=>24,'jumlah_harga'=>420000]);
        $units = $costs-> Units;
        //  dd($transaction);
        return view('production_costs.show',['costs'=>$costs,'transaction'=>$transaction, 'units'=>$units]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductionCost  $productionCost
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductionCost $productionCost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductionCost  $productionCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductionCost $productionCost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductionCost  $productionCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $deleteItem = ProductionCost::find($request-> cost_id);
        $deleteProduk= $deleteItem->Units()->detach();
        // dd($deleteItem);
        if ($deleteItem->delete()) {
            
            return redirect()->route('costs.index')
            ->with('success', 'Production cost deleted successfully');
        }

        return back()->withInput()->with('error','operation failed');
    }

    public function unitPrice(Request $request)
    {

     $p = UnitProduct::select('price')->where('id',$request->id)->first();
    //  dd($p);
     return response()->json($p);

    }

    public function transactionData(Request $request)
    {

     $p = Transaction::select('total_order')->where('id',$request->id)->first();
    //  dd($p);
     return response()->json($p);

    }
}
