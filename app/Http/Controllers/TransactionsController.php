<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Product;
use App\Customer;
use App\ProductionCost;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $transaction = 4;
        // $costs = ProductionCost::where('order_id',$transaction)->get();
        // dd($costs);
        // dd(Auth::user()->id);
        $transactions = Transaction::all();
        // dd($transactions);
        return view('transactions.index',['transactions'=> $transactions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rand = str_random(8);
        $invoice = 'SF'.$rand;
        $products = Product::all();
        $customers = Customer::all();

        // dd($customers);
        return view('transactions.create',['invoices'=>$invoice, 'customers'=>$customers, 'products'=>$products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $biaya = $request->input('bayar');
        $user= Auth::user()->id;
        $status="Dalam Proses";

        // $number = count($_POST["productname"]);
        // dd($number);
        $transactions = Transaction::create([
            'invoice_no' => $request->input('no_invoice'),
            'order_name' => $request->input('order_name'),
            'user_id' => $user,
            'customer_id' => $request->input('customer'),
            'total_order' => $request->input('biaya_total'),
            'payment' => $request->input('bayar'),
            'kurang' => $request->input('kurang'),
            'notes' => $request->input('notes'),
            'status' => $status,
            'tanggal_jadi' => $request->input('tanggal_jadi')
        ]);

        $transaction = Transaction::orderBy('created_at', 'desc')->first();
        $id_transaction = $transaction-> id;

        $forProduct= Transaction::findOrFail($id_transaction);
        // dd($transaction);
        // if($number > 0)  
        // {  
        //     for($i=0; $i<$number; $i++)  
        //     { 
        //         $products=$forProduct->Products()->attach($_POST['product_name'][$i],['quantity'=>$_POST['quantity'][$i],'jumlah_harga'=>$_POST['jumlah_harga'][$i]]);
        //     }
        // }
        
        for ($i = 1; $i <= count($request->productname); $i++) {
            if (!empty($request->productname[$i])) {
            $products=$forProduct->Products()->attach($request->productname[$i],['quantity'=>$request->quantity[$i],'jumlah_harga'=>$request->jumlah_harga[$i]]);
            }
        }

        // return redirect() -> route('transactions.index');
        // return redirect('transactions')->with('success', 'Your order successfully submitted');
        if($transactions){
            return redirect() -> route('transactions.show', $id_transaction)
            -> with('success', 'Product created successfully');
        }
        return back() -> withInput() -> with('errors','operation failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($transaction)
    {
        $transactions = Transaction::findorfail($transaction);
        // $detail = $transactions-> DetailOrder;
        $customer = $transactions-> Customer;
        $user = $transactions-> User;
        // $product = Product::findorfail($detail);

        // $products = DetailOrder::find(1);
        // $product = $transactions->Products()->attach(1,['quantity'=>4,'jumlah_harga'=>220000]);
        $product = $transactions-> Products;
        // $unit = $transactions-> Products->first()-> UnitProduct;


        
        // foreach ($transactions->Products as $product) {
        //    echo $product->pivot->quantity;
          
        //    echo $product->pivot->jumlah_harga;

        // }
        // dd($products);
        return view('transactions.show',['transactions'=>$transactions,'customers'=>$customer, 'users'=>$user, 'products'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $transaction)
    {        

        $costs = ProductionCost::where('order_id',$transaction)->get();
        // dd($costs);

        if ($costs->isNotEmpty()) {
            $itemUpdate = Transaction::where('id',$transaction)
            ->update([
            'status' => $request->input('status')
            ]);
        } else {
            $message = "belum belanja";
            echo "<script type='text/javascript'>alert('$message');</script>";
            // return back()->withInput();
            return redirect()->route('transactions.index')->with('error','belum belanja');
        }
       
        if ($itemUpdate) {
            return redirect()->route('transactions.index')
            ->with('success', 'Order diambil Successfully');
        }
        
        return back()->withInput()->with('error','belum belanja');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $deleteItem = Transaction::find($request-> order_id);
        $deleteProduk= $deleteItem->Products()->detach();
        // dd($deleteItem);
        if ($deleteItem->delete()) {
            
            return redirect()->route('transactions.index')
            ->with('success', 'Transaction deleted successfully');
        }

        return back()->withInput()->with('error','operation failed');
    }

    public function findPrice(Request $request)
    {

     $p = Product::select('price')->where('id',$request->id)->first();
    //  dd($p);
     return response()->json($p);

    }

    public function invoice(){
        return view('transactions.invoice');
    }
}
