<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = DB::table('customers')->get();

        return view('customers.index', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = Customer::create([
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone'),            
        ]);

        if ($customer) {
            # code...
            return redirect()->route('customers.index')
            ->with('success', 'Customer created successfully');
        }
        return back()->withInput()->with('errors', 'operation failed');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $customers = Customer::find($customer -> id);
        // dd($customers);
        return view('customers.show', ['customers' => $customers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        $customers = Customer::find($customer -> id);
        return view('customers.edit', ['customers' => $customers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $customerUpdate = Customer::where('id', $customer-> id)
        -> update([
            'name' => $request->input('name'),
            'address' => $request->input('address'),
            'phone' => $request->input('phone')
        ]);
        
        if ($customerUpdate) {
            return redirect()->route('customers.show', ['customer' => $customer-> id])
            ->with('success', 'Update Successfully');
        }
        
        return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request-> customer_id);
        $deleteCustomer = Customer::find($request-> customer_id);

        if ($deleteCustomer->delete()){            
            return redirect()->route('customers.index')
            ->with('success', 'Customer deleted successfully');
        }

        return back()->withInput()->with('error','operation failed');

    }
}
