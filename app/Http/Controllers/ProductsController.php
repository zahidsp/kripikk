<?php

namespace App\Http\Controllers;

use App\Product;
use App\UnitProduct;
use App\Transaction;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $transaction = Transaction::orderBy('created_at', 'desc')->first();
        // $id_transaction = $transaction-> id;
        // dd($id_transaction);
        $products = Product::all();
        return view('products.index',['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $units = UnitProduct::all();
        return view('products.create',['units'=>$units]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products = Product::create([
            'name' => $request->input('name'),
            'unit_id' => $request->input('unit'),
            'price' => $request->input('price')
        ]);

        if($products){
            return redirect() -> route('products.index')
            -> with('success', 'Product created successfully');
        }
        return back() -> withInput() -> with('errors','operation failed');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($product)
    {
        
        $products = Product::findorfail($product);

        $unit = $products -> UnitProduct;
       return view('products.show',['products' => $products, 'unit' => $unit]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($product)
    {
        $products = Product::findorfail($product);
        $units = UnitProduct::all();
        $unit = $products -> UnitProduct;
        // dd($unit);
        return view('products.edit',['products' => $products, 'unit'=> $unit, 'units'=> $units]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $product)
    {
        $itemUpdate = Product::where('id', $product)
        ->update([
            'name' => $request->input('name'),
            'unit_id' => $request->input('unit'),
            'price' => $request->input('price')
        ]);

        if ($itemUpdate) {
            return redirect()->route('products.index')
            ->with('success', 'Update Successfully');
        }
        
        return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $deleteItem = Product::find($request-> product_id);
        // dd($deleteItem);
        if ($deleteItem->delete()) {
            
            return redirect()->route('products.index')
            ->with('success', 'Item Produk deleted successfully');
        }

        return back()->withInput()->with('error','operation failed');
    }

}
