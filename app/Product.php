<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Product extends Model
{
    protected $fillable = [
        'id',
        'name',        
        'unit_id',
        'price'
    ];

    public function UnitProduct()
    {
        return $this->belongsTo('App\UnitProduct','unit_id');
    }

    public function DetailOrders()
    {
        return $this->belongsToMany('App\DetailOrder','order_product');
    }

    public function Transactions()
    {
        return $this->belongsToMany('App\Transaction','transaction_product');
    }
}
