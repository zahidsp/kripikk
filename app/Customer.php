<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'id',
        'name',
        'address',
        'phone'
    ];

    public function Transactions(){
        return $this->hasMany('App\Transaction','customer_id');
    }
}
