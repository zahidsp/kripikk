<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'id',
        'invoice_no',
        'order_name',
        'user_id',
        'customer_id',
        'total_order',
        'payment',
        'kurang',
        'notes',
        'status',
        'tanggal_jadi'
    ];

    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function Customer()
    {
        return $this->belongsTo('App\Customer','customer_id');
    }
    
    public function Products()
    {
        return $this->belongsToMany('App\Product', 'transaction_product')
        ->withTimeStamps()
        ->withPivot(['quantity','jumlah_harga']);
    }

    public function Cost(){
        return $this->hasMany('App\ProductionCost','order_id');
    }
}
