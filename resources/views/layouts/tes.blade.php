@extends('layouts.root')

@section('content')
<div style="width:520px;margin:0px outo;margin-top:30px;">

        <select id="e3" style="width:100px;">
        <option>teh</option>
        <option>teah anget</option>
        <option>teh thai</option>
        <option>teh es</option>
        </select>
       </div>
       
       
       <script>
            $(document).ready(function() { $("#e3").select2(); });
        </script>


<br />  
<br />  
<h2 align="center">Dynamically Add or Remove input fields in PHP with JQuery</h2>  
<div class="form-group">  
     <form name="add_name" id="add_name">  
          <div class="table-responsive">  
               <table class="table table-bordered" id="dynamic_field">  
                    <tr>  
                         <td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td>  
                         <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                    </tr>  
               </table>  
               <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  
          </div>  
     </form>  
</div>  
</div>  

<script>  
                $(document).ready(function(){  
                     var i=5; 
                     for (let index = 0; index < i; index++) {
                          $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');                     
                     } 
                    
                });  
                </script>
                  
@endsection