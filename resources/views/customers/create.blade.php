@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Tambah Customer</h4>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                    <form 
                    action="{{ route('customers.store') }}" 
                    method="POST">
                            {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" 
                                    placeholder="Inputkan Nama Customer"
                                    name="name"
                                    required>
                                </div>
                            </div>
                          
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Nomor HP <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="phone"
                                    placeholder="Inputkan Nomor HP Customer"
                                    name="phone"
                                    required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Alamat <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="address"
                                    placeholder="Inputkan Alamat Customer"
                                    name="address"
                                    required>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>

            </div>
        </div>
    </div>
</div>
@endsection