@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Biaya Produsi Baru</h4>

        </div>
    </div>
</div>

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
          
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <form name="add_cost" id="add_cost"
                        action="{{ route('costs.store') }}"
                        method="Post">
                            {{ csrf_field() }}

                            <table class="table table-borderless" id="transactions" style="margin-bottom: 80px;" >
                                <tr >
                                    {{-- <div class="form-group row"> --}}
                                        <td width="10%">
                                            <label class="text-right control-label col-form-label">Pilih Order</label>
                                        </td>
                                        <td width="80%">

                                            <select class="form-control" name="transaction" id="transaction">
                                                    <option value="" selected>---pilih---</option>
                                                @foreach ($transactions as $transaction )
                                                <option value="{{ $transaction-> id }}">{{ $transaction-> order_name }}</option>
                                                @endforeach
                                            </select>                                       

                                            <script>
                                                $(document).ready(function() { $("#transaction").select2(); });
                                            </script>

                                        </td>
                                        
                                    {{-- </div> --}}
                                </tr>
                                <tr>
                                    <td width="10%">
                                        <label class="text-right control-label col-form-label">Total Biaya Order</label>
                                    </td>
                                    <td width="80%">
                                        <input type="text" name="total_transaksi" id="total_transaksi" readonly/>
                                    </td>  
                                </tr>
                            </table>

                            <span>
                                <h4>
                                    Detail Unit
                                </h4>
                            </span>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dynamic_field" style="margin-bottom: 80px;">
                                    <tr id="unit" class="unit">
                                        <td>
                                            <label fclass="text-right">Nama Produk</label>
                                            <select class="unitname" id="unitname">
                                                    <option value="" selected>---pilih---</option>
                                                @foreach ($units as $unit )
                                                <option value="{{ $unit-> id }}">{{ $unit-> name }}</option>
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <label class="text-right">Harga per Kg</label>
                                            <input type="text" id="harga_satuan" class="form-control "
                                                placeholder="Harga per Kg" readonly />
                                        </td>

                                        <td>
                                            <label class="text-right ">Quantity (Kg)</label>
                                            <input type="text" id="quantity" class="form-control"
                                                placeholder="Qauntity" />
                                        </td>

                                        <td>
                                            <label class="text-right ">Jumlah Harga</label>
                                            <input type="text" id="jumlah_harga" class="form-control"
                                                placeholder="Jumlah Harga" readonly />
                                        </td>

                                        <td>
                                            <button type="button" name="add" id="add" class="btn btn-success">Tambah</button>
                                        </td>
                                    </tr>


                                </table>

                                
                                <span>
                                    <h4>
                                        Detail Biaya
                                    </h4>
                                </span>

                                <table class="table " id="biayaOrder" >
                                    <tr>
                                        <td>
                                            <label class="text-right ">Subtotal</label>
                                            <input type="text" class="form-control" id="subtotal" name="subtotal"
                                                readonly>
                                        </td>
                                        <td>
                                            <label class="text-right ">Diskon</label>
                                            <input type="text" class="form-control" id="diskon" placeholder="Inputkan jumlah diskon"
                                                name="diskon">
                                        </td>
                                        <td>
                                            <label class="text-right ">Total</label>
                                            <input type="text" class="form-control" id="total" name="total" readonly>
                                        </td>
                                    </tr>

                                    <input type="hidden" name="potensi_laba" id="potensi_laba"/>

                                </table>
                                {{-- <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" /> --}}
                                <div class="border-top">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>



</div>

<script>
$('#add_cost').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
</script>
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    var namaProduk;
    var biayaTotal = 0;
    var hargaSatuan = 0;
    var quantity = 0;
    var total = 0;
    var diskon = 0;
    var total = 0;
    var total_transaksi = 0;
    var potensi_laba = 0;

    $(document).ready(function () {
        var a = $("#unit");
        var b = $("#biayaOrder");
        var c = $("#transactions");

        $("#quantity").keyup(function (event) {
            if (event.keyCode === 13) {
                // alert('Quantity key pressed.');

                namaProduk = $('#unitname').val()
                hargaSatuan = $('#harga_satuan').val();
                quantity = $('#quantity').val();
                total = quantity * hargaSatuan;

                //     console.log("unit e")
                // console.log(namaProduk);
                // console.log(hargaSatuan);
                // console.log(quantity);
                // console.log(total);

                a.find('#jumlah_harga').val(total);
                
            }
        });

        $("#diskon").keyup(function (event) {
            if (event.keyCode === 13) {
                // alert('diskon key pressed.');
                
                diskon = $("#diskon").val();

                total_transaksi = $("#total_transaksi").val();
                total = biayaTotal - diskon;
                potensi_laba = total_transaksi-total;


                // alert('enter key pressed.');
                // console.log(total);

                b.find('#total').val(total);
                b.find('#potensi_laba').val(potensi_laba);

                //call you ajax() here
            }
        });
    });
</script>

{{-- <script>
    var diskon = 0;
    var total = 0;
    $(document).ready(function () {
        var a = $("#biayaOrder");
        var $selector2 = $('#diskon');
        $(document.body).off('keyup', $selector2);

        $(document.body).on('keyup', $selector2, function (event) {
            if (event.keyCode == 13) {
                diskon = $($selector2).val();
                total = biayaTotal - diskon;

                alert('enter key pressed.');
                console.log(total);

                a.find('#total').val(total);
            }
        });
    });
</script> --}}

<script>
    $(document).ready(function () {
        $("#unitname").select2();
    });
</script>

<script>
    $(document).ready(function () {
        $(document).on('change', '#transaction', function () {

            var trans_id = $(this).val();
            var a = $("#transactions");
            // console.log(prod_id);
            // console.log(a);
            var op = "";
            // a.find('#total_transaksi').val("datatata");

            $.ajax({
                type: 'get',
                url: '{!!URL::to('transactionData')!!}',
                data: {
                    'id': trans_id
                },
                dataType: 'json', 
                success: function (data) {
                    // console.log("price");
                    // console.log(data.order_name);
                    a.find('#total_transaksi').val(data.total_order);
                },
            })
        });

        $(document).on('change', '#unitname', function () {

            var prod_id = $(this).val();
            var a = $("#unit");
            // console.log(prod_id);
            // console.log(a);
            var op = "";

            $.ajax({
                type: 'get',
                url: '{!!URL::to('unitPrice')!!}',
                data: {
                    'id': prod_id
                },
                dataType: 'json', //return data will be json
                success: function (data) {
                    // console.log("price");
                    // console.log(data.price);
                    a.find('#harga_satuan').val(data.price);
                },
            })
        });
    });
</script>

<script>
    var i = 0;
    $(document).ready(function () {
        $('#add').click(function () {
            i++;
            // $('#dynamic_field').append('<tr id="unit'+i+'" class="unit'+i+'"> <td> <label class="text-right">Nama Produk</label> <input type="text" id="namaProduk'+i+'" name="namaProduk[]" class="form-control" readonly/> </td> <td> <label class="text-right"> Harga Satuan</label><input type="text" id="harga_satuan'+i+'" name="harga_satuan[]" class="form-control" readonly/> </td> <td> <label class="text-right ">Quantity</label> <input type="text" id="quantity'+i+'" name="quantity[]" class="form-control" readonly/> </td> <td> <label class="text-right">Jumlah Harga</label><input type="text" id="jumlah_harga'+i+'" name="jumlah_harga[]" class="form-control" readonly/> </td> <td> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Hapus</button></td></tr>');
            $('#dynamic_field').append('<tr id="unit' + i + '" class="unit' + i +
                '"> <td> <label class="text-right">Nama Produk</label> <select class="unitname" id="namaProduk' +
                i +
                '" name="unitname['+ i +']" style="pointer-events: none;"> @foreach ($units as $unit ) <option value="{{ $unit-> id }}">{{ $unit-> name }}</option> @endforeach </select></td> <td> <label class="text-right"> Harga Satuan</label><input type="text" id="harga_satuan' +
                i +
                '" name="harga_satuan['+ i +']" class="form-control" readonly/> </td> <td> <label class="text-right ">Quantity</label> <input type="text" id="quantity' +
                i +
                '" name="quantity['+ i +']" class="form-control" readonly/> </td> <td> <label class="text-right">Jumlah Harga</label><input type="text" id="jumlah_harga' +
                i +
                '" name="jumlah_harga['+ i +']" class="form-control" readonly/> </td> <td> <button type="button" name="remove" id="' +
                i + '" class="btn btn-danger btn_remove">Hapus</button></td></tr>');

            var a = $("#unit" + i);
            var b = $("#biayaOrder");

            a.find('#namaProduk' + i).val(namaProduk);
            a.find('#harga_satuan' + i).val(hargaSatuan);
            a.find('#quantity' + i).val(quantity);
            a.find('#jumlah_harga' + i).val(total);

            biayaTotal = biayaTotal + total;
            b.find('#subtotal').val(biayaTotal);

            // console.log(biayaTotal);
            // var b ="#unit"+i;

            // $("b option:not(:selected)").prop("disabled", true);

        });
        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#unit' + button_id + '').remove();
        });
        // $('#submit').click(function () {
        //     $.ajax({
        //         url: "{{ route('costs.store') }}",
        //         method: "POST",
        //         data: $('#add_cost').serialize()
        //         success: function (data) {
        //             alert("suksess");
        //             $('#add_cost')[0].reset();  
        //         }
        //     });
        // });
    });
</script>

@endsection