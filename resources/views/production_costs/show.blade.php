@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Detail Biaya Produksi</h4>
          
        </div>
    </div>
</div>

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
               
                    <form 
                    {{-- action="{{ route('costs.edit', [$costs -> id]) }}" --}}
                     method="get">
                            {{ csrf_field() }}
                        <div class="card-body">

                            <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Oder</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="name" placeholder="First Name Here"
                                        name="order_name"
                                        value="{{ $costs -> order_name }}"
                                        readonly>
                                    </div>
                                </div>
                            {{-- <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Order</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" placeholder="First Name Here"
                                    name="order_name"
                                    value="{{ $transaction -> order_name }}"
                                    readonly>
                                </div>
                            </div> --}}
                          
                           

                            <div class="table-responsive">
                                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                                       
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table  class="table table-striped table-bordered" role="grid"
                                                    aria-describedby="zero_config_info">
                                                    <thead>
                                                        <tr role="row">
                                                            <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                                colspan="1" aria-sort="ascending" aria-label="Nomor: activate to sort column descending"
                                                                style="width: 10px;">No</th>
                                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                                colspan="1" aria-label="Nama: activate to sort column ascending" style="width: 80px;">
                                                                Nama Unit</th>
                                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                                colspan="1" aria-label="Alamat: activate to sort column ascending"
                                                                style="width: 80px;">Harga Satuan</th>
                                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                                Jumlah</th>
                                                            
                                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                                Jumlah Harga</th>            
                                                        </tr>
                                                    </thead>
        
                                                    <tbody>
                                                        <?php $urut=0; ?>
                                                      
                                                        @foreach ($units as $unit)
                                                        <?php $urut++; ?>
                                                        <tr role="row" class="odd">
                                                            <td class="sorting_1">
                                                                <?php echo $urut; ?>
                                                            </td>
                                                            <td>{{ $unit -> name }}</td>
                                                            <td>{{ $unit -> price }}</td>
                                                            <td>{{ $unit-> pivot-> quantity }}</td>
                                                            <td>{{ $unit-> pivot-> jumlah_harga }}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                
                                                  
                                            
                                                </table>

                                                <div class="form-group row">
                                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Subtotal</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="name" placeholder="First Name Here"
                                                        name="total"
                                                        value="{{ $costs -> subtotal }}"
                                                        readonly>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Diskon</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="name" placeholder="First Name Here"
                                                        name="total"
                                                        value="{{ $costs -> diskon }}"
                                                        readonly>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Biaya</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="name" placeholder="First Name Here"
                                                        name="total"
                                                        value="{{ $costs -> total_cost }}"
                                                        readonly>
                                                    </div>
                                                </div>

                                                {{-- <div class="form-group row">
                                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Potensi Laba</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="potensi_laba" placeholder="First Name Here"
                                                        name="potensi_laba"
                                                        value="{{ $costs -> potensi_laba }}"
                                                        readonly>
                                                    </div>
                                                </div> --}}
        
                                            </div>
                                        </div>
                
                                    </div>
                                </div>

                        </div>
                        {{-- <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </div> --}}
                    </form>               

            </div>

        </div>
    </div>

    

</div>

<script>  
        $(document).ready(function(){  
             var i=1;  
             $('#add').click(function(){  
                  i++;  
                  $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');  
             });  
             $(document).on('click', '.btn_remove', function(){  
                  var button_id = $(this).attr("id");   
                  $('#row'+button_id+'').remove();  
             });  
             $('#submit').click(function(){            
                //   $.ajax({  
                //        url:"name.php",  
                //        method:"POST",  
                //        data:$('#add_name').serialize(),  
                //        success:function(data)  
                //        {  
                //             alert(data);  
                //             $('#add_name')[0].reset();  
                //        }  
                //   });  
             });  
        });  
        </script>

@endsection
