@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Laporan Bulan <?php echo $tes ?></h4>
            {{-- <div class="ml-auto text-right">
                <a href="/transactions/create"><button type="button" class="btn btn-success">Tambah Transaksi</button></a>                      
            </div> --}}
        </div>
    </div>
</div>

<div class="container-fluid">
    
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">


                    {{-- <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Waktu</label>
                            <div class="col-sm-9">
                                <select name="time" id="time">
                                    <option value="1">Januari</option>
                                    <option value="1">Februari</option>
                                    <option value="1">Maret</option>
                                    <option value="1">April</option>
                                    <option value="1">Mei</option>
                                </select>
                            </div>
                        </div> --}}

                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="zero_config" class="table table-striped table-bordered dataTable" role="grid"
                                    aria-describedby="zero_config_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-sort="ascending" aria-label="Nomor: activate to sort column descending"
                                                style="width: 10px;">No</th>
                                            {{-- <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Nama: activate to sort column ascending" 
                                                style="width: 80px;">No. Invoice</th> --}}
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Alamat: activate to sort column ascending"
                                                style="width: 80px;">Nama Order</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Tanggal Ambil</th>
                                            
                                          
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Total Order</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Total Produksi</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Keuntungan</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $urut=0; ?>
                                        @foreach ($transactions as $transaction)
                                        <?php $urut++; ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <?php echo $urut; ?>
                                            </td>
                                            {{-- <td>{{ $transaction -> invoice_no }}</td> --}}
                                            <td>{{ $transaction -> order_name }}</td>
                                            <td>{{ $transaction -> updated_at }}</td>
                                            <td>{{ $transaction -> total_order}}</td>
                                            <td>{{ $transaction -> cost[0]['total_cost'] }}</td>
                                            <td>{{ $transaction -> cost[0]['potensi_laba'] }}</td>
                                        </tr>
                                        
                                        @endforeach
                                    </tbody>
                              
                                    {{-- <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Nomor</th>
                                            <th rowspan="1" colspan="1">Nama</th>
                                            <th rowspan="1" colspan="1">Alamat</th>
                                            <th rowspan="1" colspan="1">No. HP</th>
                                            <th rowspan="1" colspan="1">Aksi</th>
                                        </tr>
                                    </tfoot> --}}
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row d-print-none">
                        <div class="col-12">

                          <a href="" target="_blank" class="btn btn-default" id="print"><i class="fa fa-print"></i> Print</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
        $(document).ready(function () {        
           $('#print').click(function () {
               
               window.print();
           });//iki bates func
       });
       </script>
@endsection