@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Laporan</h4>
            {{-- <div class="ml-auto text-right">
                <a href="/transactions/create"><button type="button" class="btn btn-success">Tambah Transaksi</button></a>                      
            </div> --}}
        </div>
    </div>
</div>

<div class="container-fluid">
    
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                    <div class="form-group row">
                            <label class="col-sm-3 text-right control-label col-form-label">Pilih Bulan</label>
                            
                            <form name="" id=""
                            action="{{ route('reports.store') }}"
                            method="Post">
                                {{ csrf_field() }}

                                <div class="col-sm-9">
                                    <select name="month" id="month" class="form-control">
                                        <option value="" selected>---pilih---</option>
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>

                                    <script>
                                            $(document).ready(function() { $("#month").select2(); });
                                        </script>
                                </div>

                                
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary pull-center"> Pilih </button>
                                    </div>
                                </div>
                            
                            </form>
                            
                        </div>

                
            </div>
        </div>
    </div>
</div>
</div>


@endsection