@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Data Transaksi</h4>
            <div class="ml-auto text-right">
                <a href="/transactions/create"><button type="button" class="btn btn-success">Tambah Transaksi</button></a>                      
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="zero_config" class="table table-striped table-bordered dataTable" role="grid"
                                    aria-describedby="zero_config_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-sort="ascending" aria-label="Nomor: activate to sort column descending"
                                                style="width: 10px;">No</th>
                                            {{-- <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Nama: activate to sort column ascending" 
                                                style="width: 80px;">No. Invoice</th> --}}
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Alamat: activate to sort column ascending"
                                                style="width: 80px;">Nama Order</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Tanggal Order</th>
                                            
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Tanggal Jadi</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Total</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Kurang</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="No HP: activate to sort column ascending" style="width: 50px;">
                                                Status</th>
                                            <th class="sorting" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Aksi" style="width: 30px;">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $urut=0; ?>
                                        @foreach ($transactions as $transaction)
                                        <?php $urut++; ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <?php echo $urut; ?>
                                            </td>
                                            {{-- <td>{{ $transaction -> invoice_no }}</td> --}}
                                            <td>{{ $transaction -> order_name }}</td>
                                            <td>{{ $transaction -> created_at }}</td>
                                            <td>{{ $transaction -> tanggal_jadi }}</td>
                                            <td>{{ $transaction -> total_order }}</td>
                                            <td>{{ $transaction -> kurang }}</td>
                                            <td>{{ $transaction -> status }}</td>

                                            <td>
                                                    <ul class="d-flex justify-content-center" style=" list-style-type: none;">
                                                        <li class="mr-3"><a href="/transactions/{{ $transaction -> id }}" ><button type="button" class="btn btn-primary btn-sm">Detail</button></a></li>
                                                            
                                                            {{-- <li class="mr-3"><a href="/transactions/{{ $transaction -> id }}/edit"><button type="button" class="btn btn-info btn-sm">Edit</button></a></li> --}}
                                                            
                                                            <li>
                                                                    <button class="btn btn-danger btn-sm" 
                                                                    data-catid= {{ $transaction-> id}} 
                                                                    data-toggle="modal" 
                                                                    data-target="#delete">Delete</button>
                                                            </li>
                                                           
                                                        </ul>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                              
                                    {{-- <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Nomor</th>
                                            <th rowspan="1" colspan="1">Nama</th>
                                            <th rowspan="1" colspan="1">Alamat</th>
                                            <th rowspan="1" colspan="1">No. HP</th>
                                            <th rowspan="1" colspan="1">Aksi</th>
                                        </tr>
                                    </tfoot> --}}
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title text-center">Delete Confirmation</h4>
            </div>
            <form 
            action="{{ route( 'transactions.destroy','tes') }}" 
            method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                <div class="modal-body">
                      <p class="text-center">
                          Yakin ingin menghapus data ini?
                      </p>
                        <input type="hidden" name="order_id" id="cat_id" value="">
      
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-warning">Hapus</button>
                </div>
            </form>
          </div>
        </div>
      </div>
@endsection