@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Transaksi Baru</h4>

        </div>
    </div>
</div>

<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            {{-- <div class="card">

                <form action="{{ route('transactions.edit', [$transactions -> id]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">No Invoice</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" placeholder="First Name Here" name="order_name"
                                    value="{{ $invoices }}" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Order</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="order_name" placeholder="Inputkan nama order"
                                    name="order_name">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama Customer
                                <span class="required">*</span></label>
                            <div class="col-sm-9">

                                <select id="e3" class="form-control" name="unit">
                                    @foreach ($customers as $customer )
                                    <option value="{{ $customer-> id }}">{{ $customer-> name }}</option>
                                    @endforeach
                                </select>

                                <script>
                                    $(document).ready(function() { $("#e3").select2(); });
                                        </script>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Tanggal Jadi</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" id="tanggal_jadi" name="tanggal_jadi">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Catatan</label>
                            <div class="col-sm-9">
                                <textarea rows="4" cols="50" class="form-control" id="notes" name="notes" placeholder="Tambahkan catatan disini"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </div>
                    </div>
                </form>

            </div> --}}

            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <form name="add_order" id="add_order"
                        action="{{ route('transactions.store') }}"
                        method="Post">
                            {{ csrf_field() }}

                            <table class="table table-borderless" style="margin-bottom: 80px;">
                                <tr>
                                    {{-- <div class="form-group row"> --}}
                                        <td width="10%">
                                            <label class="text-right control-label col-form-label">No Invoice</label>
                                        </td>
                                        <td width="80%">

                                            <input type="text" class="form-control" id="name" placeholder="First Name Here"
                                                name="no_invoice" value="{{ $invoices }}" readonly>

                                        </td>
                                    {{-- </div> --}}
                                </tr>
                                <tr>
                                    {{-- <div class="form-group row"> --}}
                                        <td width="15%">
                                            <label class="text-right control-label col-form-label">Nama Order</label>
                                        </td>
                                        <td width="80%">

                                            <input type="text" class="form-control" id="order_name" placeholder="Inputkan nama order"
                                                name="order_name">

                                        </td>
                                    {{-- </div> --}}
                                </tr>
                                <tr>
                                    {{-- <div class="form-group row"> --}}
                                        <td width="10%">
                                            <label class="text-right control-label col-form-label">Customer</label>
                                        </td>
                                        <td width="80%">

                                            <select id="e3" class="form-control" name="customer">
                                                    <option value="" selected>---pilih---</option>
                                                @foreach ($customers as $customer )
                                                <option value="{{ $customer-> id }}">{{ $customer-> name }}</option>
                                                @endforeach
                                            </select>

                                            <script>
                                                $(document).ready(function() { $("#e3").select2(); });
                                            </script>

                                        </td>
                                    {{-- </div> --}}
                                </tr>
                                <tr>
                                    {{-- <div class="form-group row"> --}}
                                        <td width="15%">
                                            <label class="text-right control-label col-form-label">Tanggal Jadi</label>
                                        </td>
                                        <td width="80%">

                                            <input type="date" class="form-control" id="tanggal_jadi" name="tanggal_jadi">

                                        </td>
                                    {{-- </div> --}}
                                </tr>

                                <tr>
                                    {{-- <div class="form-group row"> --}}
                                        <td width="15%">
                                            <label class="text-right control-label col-form-label">Catatan</label>
                                        </td>
                                        <td width="80%">

                                            <textarea rows="4" cols="50" class="form-control" id="notes" name="notes"
                                                placeholder="Tambahkan catatan disini"></textarea>

                                        </td>
                                    {{-- </div> --}}
                                </tr>
                            </table>

                            <span>
                                <h4>
                                    Detail Produk
                                </h4>
                            </span>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dynamic_field" style="margin-bottom: 80px;">
                                    <tr id="product" class="product">
                                        <td>
                                            <label fclass="text-right">Nama Produk</label>
                                            <select class="productname" id="productname">
                                                    <option value="" selected>---pilih---</option>
                                                @foreach ($products as $product )
                                                <option value="{{ $product-> id }}">{{ $product-> name }}</option>
                                                @endforeach
                                            </select>
                                        </td>

                                        <td>
                                            <label class="text-right">Harga Satuan</label>
                                            <input type="text" id="harga_satuan" class="form-control "
                                                placeholder="Harga Satuan" readonly />
                                        </td>

                                        <td>
                                            <label class="text-right ">Quantity Order</label>
                                            <input type="text" id="quantity" class="form-control"
                                                placeholder="Qauntity" />
                                        </td>

                                        <td>
                                            <label class="text-right ">Jumlah Harga</label>
                                            <input type="text" id="jumlah_harga" class="form-control"
                                                placeholder="Jumlah Harga" readonly />
                                        </td>

                                        <td>
                                            <button type="button" name="add" id="add" class="btn btn-success">Tambah</button>
                                        </td>
                                    </tr>


                                </table>

                                
                                <span>
                                    <h4>
                                        Detail Biaya
                                    </h4>
                                </span>

                                <table class="table " id="biayaOrder" >
                                    <tr>
                                        <td>
                                            <label class="text-right ">Biaya Total</label>
                                            <input type="text" class="form-control" id="biaya_total" name="biaya_total"
                                                readonly>
                                        </td>
                                        <td>
                                            <label class="text-right ">Bayar</label>
                                            <input type="text" class="form-control" id="bayar" placeholder="Inputkan jumlah pembayaran"
                                                name="bayar">
                                        </td>
                                        <td>
                                            <label class="text-right ">Kekurangan</label>
                                            <input type="text" class="form-control" id="kurang" name="kurang" readonly>
                                        </td>
                                    </tr>
                                </table>
                                {{-- <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" /> --}}
                                <div class="border-top">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

            {{-- <div class="card">
                <form method="post">
                    {{ csrf_field() }}
                    <div class="card-body" id="biayaOrder">
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Biaya Total</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="biaya_total" name="biaya_total" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Bayar</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="bayar" placeholder="Inputkan jumlah pembayaran"
                                    name="bayar">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Kekurangan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="kurang" name="kurang" readonly>
                            </div>
                        </div>

                    </div>

                </form>
            </div> --}}
        </div>
    </div>



</div>

<script>
$('#add_order').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
</script>
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script>
    var namaProduk;
    var biayaTotal = 0;
    var hargaSatuan = 0;
    var quantity = 0;
    var total = 0;
    var bayar = 0;
    var kurang = 0;

    $(document).ready(function () {
        var a = $("#product");
        var b = $("#biayaOrder");

        $("#quantity").keyup(function (event) {
            if (event.keyCode === 13) {
                // alert('Quantity key pressed.');

                namaProduk = $('#productname').val()
                hargaSatuan = $('#harga_satuan').val();
                quantity = $('#quantity').val();
                total = quantity * hargaSatuan;

                //     console.log("product e")
                // console.log(namaProduk);
                // console.log(hargaSatuan);
                // console.log(quantity);
                // console.log(total);

                a.find('#jumlah_harga').val(total);
                
            }
        });

        $("#bayar").keyup(function (event) {
            if (event.keyCode === 13) {
                // alert('Bayar key pressed.');

                bayar = $("#bayar").val();
                kurang = biayaTotal - bayar;

                // alert('enter key pressed.');
                // console.log(kurang);

                b.find('#kurang').val(kurang);

                //call you ajax() here
            }
        });
    });
</script>

{{-- <script>
    var bayar = 0;
    var kurang = 0;
    $(document).ready(function () {
        var a = $("#biayaOrder");
        var $selector2 = $('#bayar');
        $(document.body).off('keyup', $selector2);

        $(document.body).on('keyup', $selector2, function (event) {
            if (event.keyCode == 13) {
                bayar = $($selector2).val();
                kurang = biayaTotal - bayar;

                alert('enter key pressed.');
                console.log(kurang);

                a.find('#kurang').val(kurang);
            }
        });
    });
</script> --}}

<script>
    $(document).ready(function () {
        $("#productname").select2();
    });
</script>

<script>
    $(document).ready(function () {
        $(document).on('change', '#productname', function () {

            var prod_id = $(this).val();
            var a = $("#product");
            // console.log(prod_id);
            // console.log(a);
            var op = "";

            $.ajax({
                type: 'get',
                url: '{!!URL::to('findPrice')!!}',
                data: {
                    'id': prod_id
                },
                dataType: 'json', //return data will be json
                success: function (data) {
                    // console.log("price");
                    // console.log(data.price);
                    a.find('#harga_satuan').val(data.price);
                },
            })

            // console.log(i);
        });
    });
</script>

<script>
    var i = 0;
    $(document).ready(function () {
        $('#add').click(function () {
            i++;
            // $('#dynamic_field').append('<tr id="product'+i+'" class="product'+i+'"> <td> <label class="text-right">Nama Produk</label> <input type="text" id="namaProduk'+i+'" name="namaProduk[]" class="form-control" readonly/> </td> <td> <label class="text-right"> Harga Satuan</label><input type="text" id="harga_satuan'+i+'" name="harga_satuan[]" class="form-control" readonly/> </td> <td> <label class="text-right ">Quantity</label> <input type="text" id="quantity'+i+'" name="quantity[]" class="form-control" readonly/> </td> <td> <label class="text-right">Jumlah Harga</label><input type="text" id="jumlah_harga'+i+'" name="jumlah_harga[]" class="form-control" readonly/> </td> <td> <button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">Hapus</button></td></tr>');
            $('#dynamic_field').append('<tr id="product' + i + '" class="product' + i +
                '"> <td> <label class="text-right">Nama Produk</label> <select class="productname" id="namaProduk' +
                i +
                '" name="productname['+ i +']" style="pointer-events: none;"> @foreach ($products as $product ) <option value="{{ $product-> id }}">{{ $product-> name }}</option> @endforeach </select></td> <td> <label class="text-right"> Harga Satuan</label><input type="text" id="harga_satuan' +
                i +
                '" name="harga_satuan['+ i +']" class="form-control" readonly/> </td> <td> <label class="text-right ">Quantity</label> <input type="text" id="quantity' +
                i +
                '" name="quantity['+ i +']" class="form-control" readonly/> </td> <td> <label class="text-right">Jumlah Harga</label><input type="text" id="jumlah_harga' +
                i +
                '" name="jumlah_harga['+ i +']" class="form-control" readonly/> </td> <td> <button type="button" name="remove" id="' +
                i + '" class="btn btn-danger btn_remove">Hapus</button></td></tr>');

            var a = $("#product" + i);
            var b = $("#biayaOrder");

            a.find('#namaProduk' + i).val(namaProduk);
            a.find('#harga_satuan' + i).val(hargaSatuan);
            a.find('#quantity' + i).val(quantity);
            a.find('#jumlah_harga' + i).val(total);

            biayaTotal = biayaTotal + total;
            b.find('#biaya_total').val(biayaTotal);

            // console.log(biayaTotal);
            // var b ="#product"+i;

            // $("b option:not(:selected)").prop("disabled", true);

        });
        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#product' + button_id + '').remove();
        });
        // $('#submit').click(function () {
        //     $.ajax({
        //         url: "{{ route('transactions.store') }}",
        //         method: "POST",
        //         data: $('#add_order').serialize()
        //         success: function (data) {
        //             alert("suksess");
        //             $('#add_order')[0].reset();  
        //         }
        //     });
        // });
    });
</script>

@endsection