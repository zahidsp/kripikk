@extends('layouts.root')

@section('content')
<div class="container">
        <div class="row">
                <div class="col-12">

                        <!-- Main content -->
                        <div id="invoice" class="invoice p-3 mb-3">
                          <!-- title row -->
                          <div class="row">
                            <div class="col-12">
                              <h4>
                                <i class="fa fa-globe"></i> Servant Factory
                                <small class="float-right">{{ $transactions -> created_at }}</small>
                              </h4>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- info row -->
                          <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                              From
                              <address>
                                <strong>{{ $users -> name }}</strong><br>
                              </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                              To
                              <address>
                                <strong>{{ $customers -> name }}</strong><br>
                                {{ $customers -> address }}<br>
                                {{-- San Francisco, CA 94107<br> --}}
                                Phone: {{ $customers -> phone }}<br>
                                {{-- Email: john.doe@example.com --}}
                              </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                              <b>Invoice #{{ $transactions -> invoice_no }}</b><br>
                              <br>
                              <b>ID Order :</b> {{ $transactions -> id }}<br>
                              <b>Nama Order :</b> {{ $transactions -> order_name }}<br>
                              <b>Tanggal Jadi :</b> {{ $transactions -> tanggal_jadi }}<br>                            
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- Table row -->
                          <div class="row">
                            <div class="col-12 table-responsive">
                              <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Nama Produk</th>
                                  <th>Harga Satuan #</th>
                                  <th>Jumlah</th>
                                  <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                        <?php $urut=0; ?>
                                                      
                                        @foreach ($products as $product)
                                        <?php $urut++; ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <?php echo $urut; ?>
                                            </td>
                                            <td>{{ $product -> name }}</td>
                                            <td>Rp {{ $product -> price }}</td>
                                            <td>{{ $product-> pivot-> quantity }}</td>
                                            <td>Rp {{ $product-> pivot-> jumlah_harga }}</td>
                                        </tr>
                                        @endforeach
                                </tbody>
                              </table>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-6">
                              <p class="lead">Catatan:</p>
                              
                              <p>
                                    {{ $transactions -> notes }}
                              </p>
                            </div>
                            <!-- /.col -->
                            <div class="col-6">
                              {{-- <p class="lead">Tanggal Jadi {{ $transactions -> tanggal_jadi }}</p> --}}

                              <div class="table-responsive">
                                <table class="table">
                                  <tbody><tr>
                                    <th style="width:50%">Total:</th>
                                    <td>Rp {{ $transactions -> total_order }}</td>
                                  </tr>
                                  <tr>
                                    <th>Bayar</th>
                                    <td>Rp {{ $transactions -> payment }}</td>
                                  </tr>
                                  <tr>
                                    <th>Kekurangan:</th>
                                    <td>Rp {{ $transactions -> kurang }}</td>
                                  </tr>
                                  {{-- <tr>
                                    <th>Total:</th>
                                    <td>$265.24</td>
                                  </tr> --}}
                                </tbody></table>
                              </div>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- this row will not appear when printing -->
                          <div class="row d-print-none">
                            <div class="col-12">

                              <a href="" target="_blank" class="btn btn-default" id="print"><i class="fa fa-print"></i> Print</a>
                                {{-- <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" /> --}}

                              
                              <form action="{{ route('transactions.update',[$transactions -> id]) }}" method="post" style="margin-top: 5px;">
                                
                                  {{ csrf_field() }}
                                <input type="hidden" name="_method" value="put">
                                <input type="hidden" name="status" value="Selesai">
                                <button type="submit" class="btn btn-success ">
                                  <i class="fa fa-download" ></i>
                                  Order Diambil
                              </button>

                              </form>
                                

                              {{-- <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                <i class="fa fa-download"></i> Generate PDF
                              </button> --}}

                            </div>
                          </div>

                        </div>
                        <!-- /.invoice -->
                      </div>


        </div>
    </div>

    <script>
     $(document).ready(function () {        
        $('#print').click(function () {
            
            window.print();
        });//iki bates func
    });
    </script>
@endsection