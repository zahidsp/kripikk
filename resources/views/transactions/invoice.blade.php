<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
    
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
        <title>Servant Factory</title>
        <!-- Custom CSS -->
        <link href="../../assets/libs/flot/css/float-chart.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../../assets/libs/select2/dist/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="../../assets/libs/jquery-minicolors/jquery.minicolors.css">
        <link rel="stylesheet" type="text/css" href="../../assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="../../assets/libs/quill/dist/quill.snow.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" type="text/css" href="../../assets/extra-libs/multicheck/multicheck.css">
        <link href="../../assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
        <link href="../../dist/css/style.min.css" rel="stylesheet">
    
    
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
       
    </head>
<body>
    <div class="container">
        <div class="row">
                <div class="col-12">

                        <!-- Main content -->
                        <div id="invoice" class="invoice p-3 mb-3">
                          <!-- title row -->
                          <div class="row">
                            <div class="col-12">
                              <h4>
                                <i class="fa fa-globe"></i> Servant Factory
                                <small class="float-right">{{ $transactions -> created_at }}</small>
                              </h4>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- info row -->
                          <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                              From
                              <address>
                                <strong>{{ $users -> name }}</strong><br>
                                {{-- 795 Folsom Ave, Suite 600<br>
                                San Francisco, CA 94107<br>
                                Phone: (804) 123-5432<br>
                                Email: info@almasaeedstudio.com --}}
                              </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                              To
                              <address>
                                <strong>{{ $customers -> name }}</strong><br>
                                {{ $customers -> address }}<br>
                                {{-- San Francisco, CA 94107<br> --}}
                                Phone: {{ $customers -> phone }}<br>
                                {{-- Email: john.doe@example.com --}}
                              </address>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 invoice-col">
                              <b>Invoice #{{ $transactions -> invoice_no }}</b><br>
                              <br>
                              <b>ID Order :</b> {{ $transactions -> id }}<br>
                              <b>Nama Order :</b> {{ $transactions -> order_name }}<br>
                              <b>Tanggal Jadi :</b> {{ $transactions -> tanggal_jadi }}<br>                            
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- Table row -->
                          <div class="row">
                            <div class="col-12 table-responsive">
                              <table class="table table-striped">
                                <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Nama Produk</th>
                                  <th>Harga Satuan #</th>
                                  <th>Jumlah</th>
                                  <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                        <?php $urut=0; ?>
                                                      
                                        @foreach ($products as $product)
                                        <?php $urut++; ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <?php echo $urut; ?>
                                            </td>
                                            <td>Rp {{ $product -> name }}</td>
                                            <td>Rp {{ $product -> price }}</td>
                                            <td>{{ $product-> pivot-> quantity }}</td>
                                            <td>Rp {{ $product-> pivot-> jumlah_harga }}</td>
                                        </tr>
                                        @endforeach
                                </tbody>
                              </table>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <div class="row">
                            <!-- accepted payments column -->
                            <div class="col-6">
                              <p class="lead">Catatan:</p>
                              {{-- <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/credit/visa.png" alt="Visa">
                              <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/credit/mastercard.png" alt="Mastercard">
                              <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/credit/american-express.png" alt="American Express">
                              <img src="https://adminlte.io/themes/dev/AdminLTE/dist/img/credit/paypal2.png" alt="Paypal">

                              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem
                                plugg
                                dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                              </p> --}}
                              <input type="text" class="form-control" id="price"
                                        name="notes"
                                        value="{{ $transactions -> notes }}"
                                        readonly>
                            </div>
                            <!-- /.col -->
                            <div class="col-6">
                              <p class="lead">Tanggal Jadi {{ $transactions -> tanggal_jadi }}</p>

                              <div class="table-responsive">
                                <table class="table">
                                  <tbody><tr>
                                    <th style="width:50%">Total:</th>
                                    <td>Rp {{ $transactions -> total }}</td>
                                  </tr>
                                  <tr>
                                    <th>Bayar</th>
                                    <td>Rp {{ $transactions -> payment }}</td>
                                  </tr>
                                  <tr>
                                    <th>Kekurangan:</th>
                                    <td>Rp {{ $transactions -> kurang }}</td>
                                  </tr>
                                  {{-- <tr>
                                    <th>Total:</th>
                                    <td>$265.24</td>
                                  </tr> --}}
                                </tbody></table>
                              </div>
                            </div>
                            <!-- /.col -->
                          </div>
                          <!-- /.row -->

                          <!-- this row will not appear when printing -->
                          <div class="row no-print">
                            <div class="col-12">

                              <a href="" @click.prevent="printme" target="_blank" class="btn btn-default" id="print"><i class="fa fa-print"></i> Print</a>
                                {{-- <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" /> --}}

                              {{-- <button type="button" class="btn btn-success float-right">
                                  <i class="fa fa-credit-card"></i>
                                  Submit Payment
                              </button>

                              <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                <i class="fa fa-download"></i> Generate PDF
                              </button> --}}

                            </div>
                          </div>

                        </div>
                        <!-- /.invoice -->
                      </div>


        </div>
    </div>

    <script>
     $(document).ready(function () {        
        $('#print').click(function () {
            // window.print();
            var prtContent = document.getElementById("invoice");
            var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
            
            WinPrint.document.write('<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">');

            // To keep styling
            // var file = WinPrint.document.createElement("link");
            // file.setAttribute("rel", "stylesheet");
            // file.setAttribute("type", "text/css");
            // file.setAttribute("href", 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
            // WinPrint.document.head.appendChild(file);

            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        });//iki bates func
    });
    </script>
</body>
</html>