@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Data Unit Produk</h4>
            <div class="ml-auto text-right">
                <a href="/units/create"><button type="button" class="btn btn-success">Tambah Unit Produk</button></a>                      
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <div id="zero_config_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="zero_config" class="table table-striped table-bordered dataTable" role="grid"
                                    aria-describedby="zero_config_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-sort="ascending" aria-label="Nomor: activate to sort column descending"
                                                style="width: 10px;">Nomor</th>
                                            <th class="sorting" tabindex="1" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Nama: activate to sort column ascending" style="width: 80px;">Nama</th>
                                            <th class="sorting" tabindex="2" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Nama: activate to sort column ascending" style="width: 80px;">Harga per Kg</th>
                                          
                                            <th  tabindex="3" aria-controls="zero_config" rowspan="1"
                                                colspan="1" aria-label="Aksi" style="width: 30px;">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $urut=0; ?>
                                        @foreach ($units as $unit)
                                        <?php $urut++; ?>
                                        <tr role="row" class="odd">
                                            <td class="sorting_1">
                                                <?php echo $urut; ?>
                                            </td>
                                            <td>{{ $unit -> name }}</td>
                                            <td>{{ $unit -> price }}</td>
                                            <td>
                                                    <ul class="d-flex justify-content-center" style=" list-style-type: none;">
                                                        <li class="mr-3"><a href="/units/{{ $unit -> id }}" ><button type="button" class="btn btn-primary btn-sm">Detail</button></a></li>
                                                            
                                                            <li class="mr-3"><a href="/units/{{ $unit -> id }}/edit"><button type="button" class="btn btn-info btn-sm">Edit</button></a></li>
                                                            
                                                            <li>
                                                                    <button class="btn btn-danger btn-sm" 
                                                                    data-catid={{$unit-> id}} 
                                                                    data-toggle="modal" 
                                                                    data-target="#delete">Delete</button>
                                                            </li>
                                                           
                                                        </ul>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal -->
<div class="modal modal-danger fade" id="delete" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title text-center">Delete Confirmation</h4>
            </div>
            <form action="{{ route( 'units.destroy','tes') }}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                <div class="modal-body">
                      <p class="text-center">
                          Yakin ingin menghapus data ini?
                      </p>
                        <input type="hidden" name="unit_id" id="cat_id" value="">
      
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-success" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
          </div>
        </div>
      </div>
@endsection