@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Tambah Unit Produk</h4>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                    <form 
                    action="{{ route('units.store') }}" 
                    method="POST">
                            {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" 
                                    placeholder="Inputkan Nama Unit"
                                    name="name"
                                    required>
                                </div>
                            </div>
                          
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Harga per Kg
                                    <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="price"
                                    placeholder="Inputkan Harga"
                                    name="price"
                                    required>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>

            </div>
        </div>
    </div>
</div>
@endsection