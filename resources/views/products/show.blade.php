@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Item Detail</h4>
          
        </div>
    </div>
</div>

<div class="container-fluid">
    
    <div class="row">
        <div class="col-12">
            <div class="card">
               
                    <form 
                    action="{{ route('products.edit', [$products -> id]) }}"
                     method="get">
                            {{ csrf_field() }}
                        <div class="card-body">

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" 
                                    name="name"
                                    value="{{ $products -> name }}"
                                    readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Unit</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="unit" 
                                        name="unit"
                                        value="{{ $unit -> name }}"
                                        readonly>
                                    </div>
                            </div>
                          
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Harga</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="price"
                                    name="price"
                                    value="{{ $products -> price }}"
                                    readonly>
                                </div>
                            </div>

                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Edit</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>

</div>
    
@endsection