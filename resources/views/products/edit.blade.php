@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Item Detail</h4>
          
        </div>
    </div>
</div>

<div class="container-fluid">
    
    <div class="row">
        <div class="col-12">
            <div class="card">
               
                    <form 
                    action="{{ route('products.update', [$products -> id]) }}"
                     method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="put">
                        <div class="card-body">

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" 
                                    name="name"
                                    value="{{ $products -> name }}">
                                </div>
                            </div>

                            {{-- <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Unit</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="unit" 
                                        name="unit"
                                        value="{{ $unit -> name }}">
                                    </div>
                            </div> --}}

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Unit <span class="required">*</span></label>
                                <div class="col-sm-9">
                                  
                                <select id="e3" class="form-control" name="unit">
                                    <option value="{{ $unit -> id }}" selected>{{ $unit -> name }}</option>
                                        @foreach ($units as $allUnit )
                                            <option value="{{ $allUnit-> id }}">{{ $allUnit-> name }}</option>
                                        @endforeach
                                </select>                                        
                                       
                                       <script>
                                            $(document).ready(function() { $("#e3").select2(); });
                                        </script>
                                </div>
                            
                                
                        </div>
                          
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Harga</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="price"
                                    name="price"
                                    value="{{ $products -> price }}">
                                </div>
                            </div>

                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>

                  

            </div>
        </div>
    </div>

</div>
    
@endsection