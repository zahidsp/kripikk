@extends('layouts.root')

@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Tambah Item Produk</h4>
          
        </div>
    </div>
</div>

<div class="container-fluid">
    
    <div class="row">
        <div class="col-12">
            <div class="card">
                {{-- <input type="text" class="form-control" name="unit" id="unit"> --}}

                <?php
                
                foreach ($units as $key => $value) {
                    $itemResult[] = $value->name;
                }
                ?>


                {{-- <script type="text/javascript">
                    var path="{{ route('autocomplete') }}";
                    $('input.typeahead').typeahead({
                        source:function (query,process) {
                            return $.get(path,{query:name},function (data){
                                return process(data)
                            });
                        }
                    });
                </script> --}}

                
                    <form 
                    action="{{ route('products.store') }}"
                     method="Post">
                            {{ csrf_field() }}
                        <div class="card-body">

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Nama <span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="name" 
                                    name="name"
                                    placeholder="Inputkan Nama Item"                                    
                                    required>
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Unit <span class="required">*</span></label>
                                    <div class="col-sm-9">
                                      
                                    <select id="e3" class="form-control" name="unit">
                                            <option value="" selected>---pilih---</option>
                                            @foreach ($units as $unit )
                                                <option value="{{ $unit-> id }}">{{ $unit-> name }}</option>
                                            @endforeach
                                    </select>                                        
                                           
                                           <script>
                                                $(document).ready(function() { $("#e3").select2(); });
                                            </script>
                                    </div>
                                
                                    
                            </div>
                            
                          
                            <div class="form-group row">
                                <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Harga Satuan<span class="required">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="price"
                                    name="price"
                                    placeholder="Inputkan Harga Item"
                                    >
                                </div>
                            </div>

                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                   
            </div>
        </div>
    </div>
</div>
@endsection